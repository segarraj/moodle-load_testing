# Intro

A repository of load simulation scripts for Moodle at Jaume I University (https://www.uji.es/) using [k6](https://k6.io/) load testing tool. 

This load tests are a work in progress. 

Use at your own risk.

## Setup

### Create a test course

You can create a course the way you want.  Put a shortname and don't forget it. You'll use it later.

### Create test users

We're going to create Moodle users by generating a CSV and, then, using the upload users admin tool utility in Moodle.

All the users in the test uses the same password. 

```bash
export MOODLE_USER_PASSWORD=$(dd if=/dev/urandom bs=1 count=50 | xxd -p)
export MOODLE_COURSE_SHORTNAME=__PUT_THE_COURSE_SHORTNAME_HERE_
./tools/gen_test_users_csv.sh > test_users.csv
```

The username of all users folllow the following format:

test_user_$i

where $i is an integer.

Now create the users with the Upload Users functionality:

https://docs.moodle.org/37/en/Upload_users

## Run a test

Each test has its own prerequisites. If you're going to test concurrent quiz access, you'll have to create a quiz or several quizzies.

Please refer to k6 documentation in order to use it.

If you have installed k6 locally, you can adjust the environment variables in set_environment.sh and execute:

```bash
. ./tools/set_environment.sh 
k6 run quiz_test.js
```

For a more extensive testing, you can use docker o k6 cloud.

# License

This project copyright Universitat Jaume I and is licensed under GPLv3 and EUPLv1.2. You can find more info [here](https://www.uji.es/ujiapps/llicencia).