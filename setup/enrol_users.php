<?php

/**
 * Matricula usuarios en todos los cursos de pruebas.
 * 
 * Es necesario definir la variable de entorno MOODLE_DIRROOT con el dirroot de Moodle.
 */

define('MOODLE_DIRROOT', getenv('MOODLE_DIRROOT'));
define('CLI_SCRIPT', true);

require_once(MOODLE_DIRROOT . '/config.php');
require_once($CFG->libdir . '/clilib.php');

function print_help()
{

    global $argv;

    cli_writeln("Uso: {$argv[0]} --help | --usersfile");
    cli_writeln('');
    cli_writeln("    --help             Imprime este mensaje de ayuda.");
    cli_writeln("    --usersfile=file   Fichero CSV con los usuarios que queremos matricular.");
    cli_writeln('');
    cli_writeln("El fichero CSV debe tener, como mínimo, la columna username.");
    cli_writeln('');
}

$options = cli_get_params(
    [
        'help' => false,
        'usersfile' => false
    ],
    [
        'h' => 'help',
        'f' => 'usersfile'
    ]
);

if ($options[0]['help']) {
    print_help();
    exit(1);
}

if (!$options[0]['usersfile']) {
    cli_error("ERROR: Tienes que indicar --usersfile", 1);
}

if (!file_exists($options[0]['usersfile'])) {
    cli_error("ERROR. No existe el fichero " . $options['usersfile']);
}

$usernamecol = false;
$primera = true;
$fp = fopen($options[0]['usersfile'], "r");
while (($data = fgetcsv($fp)) !== false) {
    if ($primera) {
        for ($i = 0; $i < count($data); $i++) {
            if ($data[$i] === 'username') {
                $usernamecol = $i;
            }
        }
        if ($usernamecol === false) {
            fclose($fp);
            cli_error("ERROR. No hay definida columna username en el fichero");
        }
        $primera = false;
    } else {

        $user = \core_user::get_user_by_username($data[$usernamecol]);
        if (!$user) {
            cli_error("ERROR. El usuario " . $data[$usernamecol] . " no existe");
        }

        $courses = $DB->get_records_sql("SELECT id from {course} where shortname like 'test_course%'");
        foreach ($courses as $c) {
            $courseid = $c->id;
            $instances = enrol_get_instances($courseid, true);
            foreach ($instances as $i) {
                if ($i->enrol == 'manual') {
                    $instance = $i;
                    break;
                }
            }
            $ctx = \context_course::instance($courseid);
            $uji = enrol_get_plugin('manual');
            $uji->enrol_user(
                $instance,
                $user->id,
                5,
                0,           // timestart: para siempre
                0,           // timeend: para siempre
                null,        // status: ENROL_USER_ACTIVE
                false         // recovergrades: recupearar notas antiguas
            );
        }
    }
}

fclose($fp);
