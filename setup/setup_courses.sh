#!/bin/bash

# Crea cursos de prueba.

if [ -z "$MOODLE_DIRROOT" ]; then
    echo "ERROR. Debe especificar la variable de entorno MOODLE_DIRROOT." >&2
    exit 1
fi

PHP=/bin/php71
NUM_COURSES=30

for i in $(seq 1 $NUM_COURSES); do

    $PHP $MOODLE_DIRROOT/admin/tool/generator/cli/maketestcourse.php \
        --shortname=$(printf "test_course_%04d" $i) \
        --size=S \
        --fullname="$(printf "Test Course %04d" $i)" \
        --filesizelimit=$[10*1024*1024] \
        --bypasscheck \
        --quiet
        
done

