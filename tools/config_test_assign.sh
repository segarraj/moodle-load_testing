#!/bin/bash

export MOODLE_ASSIGN_TEST_FILE_PATH=$HOME/devel/moodle-load_testing/data/1MB_file.data
export MOODLE_USER_PASSWORD=
export MOODLE_WWWROOT=http://localhost:8000
export MOODLE_ASSIGN_TEST_ASSIGN_CMID=7
export MOODLE_CONCURRENT_USERS=100
