#!/bin/bash

# The number of virtual users we expect to be working.
export MOODLE_CONCURRENT_USERS=100

# The password for users.
export MOODLE_USER_PASSWORD=

# The Moodle wwwroot.
export MOODLE_WWWROOT=http://localhost:8000

# A Moodle courseid.
export MOODLE_COURSEID=2

# A Moodle cmid for a quiz.
export MOODLE_QUIZ_CMID=5

# The autosave seconds.
export MOODLE_QUIZ_AUTOSAVE_SECONDS=60

# Minimum time a user is expected to be in a quiz page.
export MOODLE_QUIZ_MIN_TIME_PER_PAGE=60

# Maximum time a user is expected to be in a quiz page.
export MOODLE_QUIZ_MAX_TIME_PER_PAGE=120
