#!/bin/bash

# Builds a docker image called ujimoodletests with tag latest. It copies all the necessary files to
# a temporary directory and then builds the image.

DIRSTOCOPY="../src ../data"
IMAGENAME="ujitests"
IMAGETAG="latest"

DESTDIR=$(mktemp -d)
if [ $? -ne 0 ]; then
    echo "ERROR. Cannot create temp directory." >&2
    exit 1
fi

trap on_error ERR

function on_error() {
    echo "ERROR. Cannot build image."
}

cp -a $DIRSTOCOPY $DESTDIR 
cp Dockerfile $DESTDIR

cd $DESTDIR
docker build -t $IMAGENAME:$IMAGETAG .

cat <<_EOF

-------------------------------------------------------------------------------

Build successfully. To run a test, please follow the setup instructions and run the test like this:

docker run $IMAGENAME:$IMAGETAG run /tmp/ujitests/src/tests/test_quiz.js
_EOF

