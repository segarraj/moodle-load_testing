import { sleep } from "k6";

class Utils {
    static sleep(fromSeconds, toSeconds) {
        return new SleepInfo();
    }

    static getRandomInteger (from, to) {
        return from + Math.round(Math.random() * (to - from));
    }
}

class SleepInfo {
    constructor() {
        this._from = null;
        this._to = null;
    }

    from(from) {
        this._from = from;
        return this;
    }

    to(to) {
        this._to = to;
        return this;
    }

    seconds() {
        const secondsToSleep = this._from + Math.random() * (this._to - this._from);
        sleep(secondsToSleep);
        return secondsToSleep;
    }
}

export {Utils};