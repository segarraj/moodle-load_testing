/**
 * Emulates a load of users doing a quiz and answering to it.
 * 
 * The scripts doesn't answer to quiz questions. It does the following:
 * 
 *     1. Authenticates in a Moodle site.
 *     2. Visits a course.
 *     3. Visits a quiz.
 *     4. Starts an attempt.
 * 
 * During the attempt, it simulates a worst case autosave (saving each MOODLE_QUIZ_AUTOSAVE_SECONDS seconds), loads
 * quiz images and clicks the next button. It does not answer the questions.
 * 
 * Environment variables:
 *
 *  MOODLE_USER_PASSWORD. The password for the users.
 *  MOODLE_WWWROOT. Moodle wwwroot without trailing dash.
 *  MOODLE_QUIZ_AUTOSAVE_SECONDS. The autosave interval.
 *  MOODLE_COURSEID. The Moodle course id.
 *  MOODLE_QUIZ_CMID. El cmid del quiz que vamos a utilizar para hacer tests.
 *  MOODLE_CONCURRENT_USERS. The number of concurrent users we want to achieve.
 *  MOODLE_USER_OFFSET. An offset given to choose users from.
 *  MOODLE_QUIZ_MIN_TIME_PER_QUESTION. The minimum time in secods that a user will be thinking in a question.
 *  MOODLE_QUIZ_MAX_TIME_PER_QUESTION. The maximum time in seconds that a user will be thunking in a question.
 *  MOODLE_NUM_OF_AVAILABLE_USERS. The number of available users to use.
 */
import { moodle } from './../moodle/moodle.js';
import { Trend } from 'k6/metrics';

let nextPageMetric = new Trend("next_page_metric");

export let options = {

  insecureSkipTLSVerify: true,

  stages: [
    // In 5 minutes, we receive all our users.
    { duration: "5m", target: parseInt(__ENV.MOODLE_CONCURRENT_USERS) },

    // We'll be 4 hours working.
    { duration: "4h", target: parseInt(__ENV.MOODLE_CONCURRENT_USERS) },

    // 10 minutes to go down.
    { duration: "10m", target: 0 },
  ],

  thresholds: {
    // We define that 99% of request must be under 4 seconds.
    'http_req_duration': ['p(99)<4000'],

    // Next page click must be under 2 seconds.
    'next_page_metric': ['p(99)<2000'],
  },

};

export function setup() {
  console.log("MOODLE_WWWROOT: " + __ENV.MOODLE_WWWROOT);
  console.log("MOODLE_COURSEID: " + __ENV.MOODLE_COURSEID);
  console.log("MOODLE_QUIZ_CMID: " + __ENV.MOODLE_QUIZ_CMID);
  console.log("MOODLE_USER_OFFSET: " + __ENV.MOODLE_USER_OFFSET);
  console.log("MOODLE_NUM_OF_AVAILABLE_USERS: " + __ENV.MOODLE_NUM_OF_AVAILABLE_USERS);
  console.log("MOODLE_CONCURRENT_USERS: " + __ENV.MOODLE_CONCURRENT_USERS);
  console.log("MOODLE_QUIZ_AUTOSAVE_SECONDS" + __ENV.MOODLE_QUIZ_AUTOSAVE_SECONDS);
  console.log("MOODLE_QUIZ_MIN_TIME_PER_QUESTION" + __ENV.MOODLE_QUIZ_MIN_TIME_PER_QUESTION);
  console.log("MOODLE_QUIZ_MAX_TIME_PER_QUESTION" + __ENV.MOODLE_QUIZ_MAX_TIME_PER_QUESTION);
}

export default () => {

  // This way of getting users should be redefined.
  let moodleUser = {
    username: 'test_user_' + (parseInt(__ENV.MOODLE_USER_OFFSET) + 1 + (parseInt(__VU) % parseInt(__ENV.MOODLE_CONCURRENT_USERS))),
    password: `${__ENV.MOODLE_USER_PASSWORD}`
  }

  let mdl = new moodle(__ENV.MOODLE_WWWROOT);
  mdl.login(moodleUser.username, moodleUser.password);
  mdl.fetchInProgressCourses();
  mdl.visitCourse(parseInt(__ENV.MOODLE_COURSEID));
  mdl.getQuizInstance(parseInt(__ENV.MOODLE_QUIZ_CMID))
    .setNextPageButtonCb((response) => {
      nextPageMetric.add(response.timings.duration);
    })
    .setLoadPageCb((contextid) => {
      mdl.fetchNotifications(contextid);
    })
    .startAttempt();
}
