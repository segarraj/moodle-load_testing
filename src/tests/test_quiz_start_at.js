/**
 * Este script simula la carga que se genera en el inicio de un cuestionario: mucha gente entre en un período de tiempo
 * corto.
 * 
 * Para ello el test da 5 minutos para crear todos los usuarios, los deja durmiendo y arranca en la hora indicada.
 * 
 *  MOODLE_USER_PASSWORD. The password for the users.
 *  MOODLE_WWWROOT. Moodle wwwroot without trailing dash.
 *  MOODLE_QUIZ_AUTOSAVE_SECONDS. The autosave interval.
 *  MOODLE_QUIZ_TIME_MIN_TIME_PER_PAGE. The minimum time in seconds that a user is going to be in a quiz page.
 *  MOODLE_QUIZ_MAX_TIME_PER_PAGE. The maximum time in seconds that a user is goinf to be in a quiz page.
 *  MOODLE_COURSEID. The Moodle course id.
 *  MOODLE_QUIZ_CMID. El cmid del quiz que vamos a utilizar para hacer tests.
 *  MOODLE_CONCURRENT_USERS. The number of concurrent users we want to achieve.
 *  MOODLE_USER_OFFSET. An offset given to choose users from.
 *  MOODLE_NUM_OF_AVAILABLE_USERS. The number of available users to use.
 *  MOODLE_RAMPUP_SECONDS. Segundos durante los cuales irán haciendo login los usuarios.
 *  MOODLE_START_AT_TIMESTAMP. Time when the users will start the quiz.
 */
import { moodle } from './../moodle/moodle.js';
import { Trend } from 'k6/metrics';
import { sleep } from 'k6';
import { Utils } from '../utils/utils.js';

let nextPageMetric = new Trend("next_page_metric");

export let options = {

  vus: __ENV.MOODLE_CONCURRENT_USERS,
  iterations: __ENV.MOODLE_CONCURRENT_USERS,

  // Ponemos una duración suficiente para aguantar el tema.
  duration: "20m",

  thresholds: {
    // We define that 99% of request must be under 4 seconds.
    'http_req_duration': ['p(99)<4000'],

    // Next page click must be under 2 seconds.
    'next_page_metric': ['p(99)<2000'],
  },

};

export function setup() {
  console.log("MOODLE_WWWROOT: " + __ENV.MOODLE_WWWROOT);
  console.log("MOODLE_COURSEID: " + __ENV.MOODLE_COURSEID);
  console.log("MOODLE_QUIZ_CMID: " + __ENV.MOODLE_QUIZ_CMID);
  console.log("MOODLE_USER_OFFSET: " + __ENV.MOODLE_USER_OFFSET);
  console.log("MOODLE_NUM_OF_AVAILABLE_USERS: " + __ENV.MOODLE_NUM_OF_AVAILABLE_USERS);
  console.log("MOODLE_CONCURRENT_USERS: " + __ENV.MOODLE_CONCURRENT_USERS);
  console.log("MOODLE_QUIZ_AUTOSAVE_SECONDS" + __ENV.MOODLE_QUIZ_AUTOSAVE_SECONDS);
  console.log("MOODLE_QUIZ_MIN_TIME_PER_QUESTION" + __ENV.MOODLE_QUIZ_MIN_TIME_PER_QUESTION);
  console.log("MOODLE_QUIZ_MAX_TIME_PER_QUESTION" + __ENV.MOODLE_QUIZ_MAX_TIME_PER_QUESTION);
  console.log("MOODLE_RAMUP_SECONDS: " + __ENV.MOODLE_RAMUP_SECONDS);
  console.log("MOODLE_START_AT_TIMESTAMP: " + __ENV.MOODLE_START_AT_TIMESTAMP);
}

export default () => {

  // This way of getting users should be redefined.
  let moodleUser = {
    username: 'test_user_' + (parseInt(__ENV.MOODLE_USER_OFFSET) + 1 + (parseInt(__VU) % parseInt(__ENV.MOODLE_CONCURRENT_USERS))),
    password: `${__ENV.MOODLE_USER_PASSWORD}`
  }

  // Llegamos en 2 minutos.
  const initSleepSeconds = Utils.sleep().from(1).from(parseInt(__ENV.MOODLE_RAMUP_SECONDS)).seconds();

  let beginTimestamp = Math.floor((new Date()) / 1000);

  let mdl = new moodle(__ENV.MOODLE_WWWROOT);
  mdl.login(moodleUser.username, moodleUser.password);
  mdl.fetchNotifications(mdl._getContextId());

  mdl.visitCourse(__ENV.MOODLE_COURSEID);
  let quiz = mdl.getQuizInstance(parseInt(__ENV.MOODLE_QUIZ_CMID))
    .setNextPageButtonCb((response) => {
      nextPageMetric.add(response.timings.duration);
    })
    .setLoadPageCb((contextid) => {
      mdl.fetchNotifications(contextid);
    });

  // Esperamos un rato hasta el momento de inicio del test.
  const startAt = new Date(parseInt(__ENV.MOODLE_START_AT_TIMESTAMP) * 1000);
  sleep(startAt/1000 - Math.floor(new Date() / 1000));

  // Al ataque!!!
  quiz.startAttempt();
  sleep(1);
}
