/**
 * Emulates a load of users doing a quiz.
 * 
 * The scripts doesn't answer to quiz questions. It does the following:
 * 
 *     1. Authenticates in a Moodle site.
 *     2. Visits a course.
 *     3. Visits a quiz.
 *     4. Starts an attempt.
 * 
 * During the attempt, it simulates a worst case autosave (saving each MOODLE_QUIZ_AUTOSAVE_SECONDS seconds), loads
 * quiz images and clicks the next button. It does not answer the questions.
 * 
 * Environment variables:
 *
 *  MOODLE_USER_PASSWORD. The password for the users.
 *  MOODLE_WWWROOT. Moodle wwwroot without trailing dash.
 *  MOODLE_CONCURRENT_USERS. The number of concurrent users we want to achieve.
 *  MOODLE_USER_OFFSET. An offset given to choose users from.
 */

import { moodle } from './../moodle/moodle.js';
import { Utils } from '../utils/utils.js';

export let options = {

  insecureSkipTLSVerify: true,

  stages: [
    // In 8 minutes, we receive all our users.
    { duration: "8m", target: parseInt(__ENV.MOODLE_CONCURRENT_USERS) },
    { duration: "1h", target: parseInt(__ENV.MOODLE_CONCURRENT_USERS) },
  ],

  thresholds: {
    // We define that 99% of request must be under 4 seconds.
    'http_req_duration': ['p(99)<4000'],
  }
};

export function setup() {
  console.log("MOODLE_WWWROOT: " + __ENV.MOODLE_WWWROOT);
  console.log("MOODLE_USER_OFFSET: " + __ENV.MOODLE_USER_OFFSET);
  console.log("MOODLE_CONCURRENT_USERS: " + __ENV.MOODLE_CONCURRENT_USERS);
}

export default () => {

  let moodleUser = {
    username: 'test_user_' + (parseInt(__ENV.MOODLE_USER_OFFSET) + 1 + (parseInt(__VU) % parseInt(__ENV.MOODLE_CONCURRENT_USERS))),
    password: `${__ENV.MOODLE_USER_PASSWORD}`
  }

  let mdl = new moodle(__ENV.MOODLE_WWWROOT);
  mdl.login(moodleUser.username, moodleUser.password);
  mdl.fetchInProgressCourses();

  for ( let i = 0; i < 2; i++) {
    let courseid = mdl.visitRandomCourse();

    let whatToDo = Utils.getRandomInteger(1, 10);
    if (whatToDo == 1) {
      // 10% de acciones de escribir en un foro.
      let forum = mdl.getRandomForum();
      forum.view();
  
      // De 10 a 20 segundos escribir un mensaje.
      Utils.sleep().from(10).to(20).seconds();
      forum.createNewDiscussion();
      Utils.sleep().from(1).to(5).seconds();
      forum.view();
  
      // DE 10 a 20 segundos responder a un mensaje.
      Utils.sleep().from(10).to(20).seconds();
      if (forum.canReplyToMessage()) {
        forum.replyToMessage();
      }
  
    } else if (whatToDo >= 2 && whatToDo <=5) {
      // 40% de acciones son de bajada de fichero.
      mdl.downloadRandomFile();
    } else {
      // 30% de acciones visitar una actividad.
      mdl.visitRandomActivity();
    }
  
    Utils.sleep().from(5).to(20).seconds();
  
    mdl.visitCourse(courseid);  
  }
  mdl.visitDashBoard();

  Utils.sleep().from(1).to(3).seconds();

  mdl.logout();
}
