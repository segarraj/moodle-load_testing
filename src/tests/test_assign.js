/**
 * Simula la entrega simultánea (aproximada) de ficheros en una tarea. MOODLE_CONCURRENT_USERS entregan a la vez en la tarea indicada.
 *
 * Environment variables:
 * 
 *      MOODLE_ASSIGN_TEST_FILE_PATH. The file path to the file we want to upload.
 *      MOODLE_USER_PASSWORD. User's password.
 *      MOODLE_wwwROOT. The Moodle's wwwroot under testing.
 *      MOODLE_ASSIGN_TEST_ASSIGN_CMID. The cmid of the assign module we want to test.
 *      MOODLE_CONCURRENT_USERS. Number of concurrent users.
 */

import { moodle } from '../moodle/moodle.js';
import { Utils } from '../utils/utils.js';
import { sleep } from 'k6';

export let options = {

    vus: __ENV.MOODLE_CONCURRENT_USERS,
    iterations: __ENV.MOODLE_CONCURRENT_USERS,
    duration: "20m",

    thresholds: {
        // We define that 99% of request must be under 4 seconds.
        'http_req_duration': ['p(99)<4000'],
    }
};

var fileToUpload = open("../../data/5MB_file.data");

export default () => {

    let moodleUser = {
        username: 'test_user_' + parseInt(__VU.toString() + __ITER.toString()),
        password: `${__ENV.MOODLE_USER_PASSWORD}`
    }

    const initSleepSeconds = Utils.sleep().from(1).from(20).seconds();

    let beginTimestamp = Math.floor((new Date()) / 1000);

    let mdl = new moodle(__ENV.MOODLE_WWWROOT);
    mdl.login(moodleUser.username, moodleUser.password);
    mdl.fetchNotifications(mdl._getContextId());

    mdl.visitCourse(__ENV.MOODLE_COURSEID);
    let assign = mdl.getAssignInstance(__ENV.MOODLE_ASSIGN_TEST_ASSIGN_CMID);
    assign.viewAssignment();

    let endTimestamp = Math.floor((new Date()) / 1000);

    sleep(120 - initSleepSeconds - ( endTimestamp - beginTimestamp) );
    assign.addSubmission(fileToUpload);
};
