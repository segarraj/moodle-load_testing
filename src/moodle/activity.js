class Activity {

    constructor(cmid) {
        this.cmid = cmid;
        this.lastResult = null;
    }

    getPageName () {
        return this.lastResult.html().find("body").attr('id');
    }
}

export { Activity };
