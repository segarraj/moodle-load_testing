import { Activity } from "./activity.js";
import { fail, check } from "k6";
import http from "k6/http";


class Forum extends Activity {

    constructor (moodle, cmid) {
        super(cmid);
        this.moodle = moodle;
    }

    view() {
        this.lastResult = http.get(this.moodle.getBaseUrl() + '/mod/forum/view.php?id=' + this.cmid);
        check(this.lastResult, {
            '200 code': (r) => r.status == 200,
        });
    }

    createNewDiscussion() {        

        if (this.getPageName() !== 'page-mod-forum-view') {
            fail('No in page-mod-forum-view page');
        }

        this.lastResult.submitForm({
            formSelector: '#collapseAddForm form',
        });
        check(this.lastResult, {
            '200 code': r => r.status == 200,
        });

        this.lastResult.submitForm({
            formSelector: '#mformforum',
            fields: {
                'subject': 'A message',
                'message[text]': 'asdñkfj añskdfjñasjdkñoiawet piqjewapf08lkqjladsjfh klajshdf lkjshfjh fjhalhsdljsd lhasd ja '
                    + 'sdfljkhasdf ljkhasdf lkjhasdf lkjhasdf lkjhasd khafl slkjad jsdlhsdlkjhsdljasf\n'
                    + 'aljkdhsafljkhasd flkjhsad lkjhasflkjhasfd ljhsdkhsf lkhadljaf kasdljasdflasf hasdhadfkjhfkjl sdf '
                    + 'lkjsadhfl kjh asdf lkjhasdf lkjhasdfljasljhaskhasdkjasfdhsfdljaflkhaf kjha jhsdfkhsflkhsadkjfllkjhasdfkjls  \n'
                    + 'FIN'
            }
        });
        check(this.lastResult, {
            '200 code': r => r.status == 200,
        });

    }

    canReplyToMessage() {
        return this.getPageName() == 'page-mod-forum-view' && this.lastResult.html().find('.discussion').size() > 0;
    }

    replyToMessage() {

        if (!this.canReplyToMessage()) {
            fail('Cannot reply to a message');
        }

        const numDiscussions = this.lastResult.html().find('.discussion').size();
        const randomDiscussion = Math.floor(numDiscussions * Math.random());

        const linkToDiscussion = this.lastResult.html().find('.discussion .topic a').eq(randomDiscussion).attr('href');
        this.lastResult = http.get(linkToDiscussion);
        check(this.lastResult, {
            '200 code': r => r.status == 200,
        });

        const numPosts = this.lastResult.html().find('.forumpost');
        const randomPost = Math.floor(numPosts * Math.random());
        const linkToReply = this.lastResult.html().find('.forumpost').eq(randomPost).find('.post-actions .btn-link[href*=reply]').attr('href');
        this.lastResult = http.get(linkToReply);
        check(this.lastResult, {
            '200 code': r => r.status == 200,
        });

        this.lastResult = this.lastResult.submitForm({
            formSelector: 'form:has(#id_message)',
            fields: {
                'message[text]': 'ñllkjasdfp9 asñdkfj lkqj3r lqjuhdlfkjhasdfkl jhasdflk jhasdkflw4yr hafluaydjlkjh askdfjh lakdhjf djho sifu '
                 + 'asdf ljkhasdf kljhasfd lkjhasdf lh hs h slhskjhgdfgskjhfkjhasdfhagsf yagsf iyso y oiuaysdf oiyasd yasdyasfaf asdf '
                 + ' akjsdfhlkjhasdf lkhjsdaf lkjh asdf lkjhasdlhadljasfjksakh jhalkh  lkdjshafljksahdf ljkhasfd lhasdlhafladfasdf '
            }
        });
        check(this.lastResult, {
            '200 code': r => r.status == 200,
        });
        
    }

}

export {Forum};
