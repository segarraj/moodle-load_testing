import { check, sleep, fail } from "k6";
import http from 'k6/http';
import { parseHTML } from "k6/html";
import { QuizResponder } from "./quizresponder.js";

class Quiz {

    constructor(moodle, cmid) {
        this.moodle = moodle;
        this.baseUrl = moodle.getBaseUrl();
        this.cmid = cmid;

        this.nextPageButtonCb = null;

        this.sesskey = null;
        this.contextid = null;
        this.currentPage = null;
    }

    startAttempt(nextButtonCb) {

        // Visit the main quiz page.
        let result = http.get(this.baseUrl + "/mod/quiz/view.php?id=" + this.cmid);
        check(result, {
            "200 code": (result) => result.status == 200,
            "Page is page-mod-quiz-view": (result) => result.html().find('body').attr('id') == 'page-mod-quiz-view',
        });

        // Extract sesskey and contextid.
        this.currentPage = parseHTML(result.body);
        this.sesskey = this.currentPage.find(".quizstartbuttondiv > form [name=sesskey]").val();

        this.contextid = this._getContextId();

        // Push the start attempts button.
        result = result.submitForm({
            formSelector: ".quizstartbuttondiv > form"
        });
        check(result, {
            "200 code": (result) => result.status == 200,
            "Page is page-mod-quiz-attempt": (result) => result.html().find('body').attr('id') == 'page-mod-quiz-attempt'
        });

        this.currentPage = parseHTML(result.body);
        while (this._currentPageIsAnAttemptPage()) {

            // Invoke the next page callback.
            this.loadPageCb && this.loadPageCb(this.contextid);

            // For each attempt page, load the quiz images.
            this._loadNonCacheableImages();

            // Click on all the links in the question text.
            this._loadLinks(result);

            // Stay n seconds here.
            let timeInthisPage = this._getTimePerPage(result);

            let elapsedTime = 0;
            let initTime = this._getNowTimestamp();
            let autoSaveIn = parseInt(__ENV.MOODLE_QUIZ_AUTOSAVE_SECONDS);
            while (elapsedTime < timeInthisPage) {
                sleep(5);
                elapsedTime = this._getNowTimestamp() - initTime;

                // Emulate autosave.
                autoSaveIn = autoSaveIn - 10;
                if (autoSaveIn <= 0) {
                    autoSaveIn = parseInt(__ENV.MOODLE_QUIZ_AUTOSAVE_SECONDS);
                    this._doAutosave();
                }
            }

            // Click the Next Page Button.
            result = this._clickNextButton(result);
            this.nextPageButtonCb && this.nextPageButtonCb(result);

            this.currentPage = parseHTML(result.body);
        }

        // We're in the final page. Now 
        if (!this._currentPageIsSummaryPage()) {
            fail('Not in the summary page: ' + this._getCurrentPageName());
        }

        // Submit all and finish.
        result.submitForm({
            formSelector: '.submitbtns form[action*=processattempt]'
        });
    }

    _loadAudio() {
        let req = [];
        this.currentPage.find('audio > source[src*=pluginfile]').each((idx, audio) => {
            req.push({
                "method": "get",
                "url": audio.getAttribute("src"),
                "params": {
                    responseType: "none",
                },
            });
        });
        return http.batch(req);
    }

    _loadVideo() {
        let req = [];
        this.currentPage.find('video > source[src*=pluginfile]').each((idx, video) => {
            req.push({
                "method": "get",
                "url": video.getAttribute("src"),
                "params": {
                    responseType: "none",
                },
            });
        });
        return http.batch(req);
    }

    _loadNonCacheableImages() {
        let req = [];
        this.currentPage.find('img[src*=pluginfile]').each((idx, img) => {
            req.push({
                "method": "get",
                "url"   : img.getAttribute("src"),
                "params": {
                    responseType: "none",
                },
            })
        });
        return http.batch(req);
    }

    _loadLinks(response) {
        this.currentPage.find('.que .qtext a').each(function (idx, elem) {
            let res = http.get(elem.getAttribute("href"), {
                "params": {
                    responseType: "none",
                },
            });
            check(res, {
                '200 is ok': (res) => res.status == 200,
            });
        });
    }

    _doAutosave() {
        let formdata = {};

        this.currentPage.find('#responseform input').each((idx, input) => {
            if (input.getAttribute('type') != 'submit') {
                formdata[input.getAttribute("name")] = input.getAttribute("value");
            }
        });

        this.currentPage.find('#responseform textarea').each((idx, textarea) => {
            formdata[textarea.getAttribute("name")] = textarea.nodeValue();
        });

        http.post(this.baseUrl + '/mod/quiz/autosave.ajax.php', formdata, {
            'Content-Type': 'application/x-www-form-urlencoded'
        });
    }

    _clickNextButton(result) {
        let responder = new QuizResponder(result);
        return result.submitForm({
            formSelector: '#responseform',
            submitSelector: '[name=next]',
            fields: responder.getResponses(),
        });        
    }

    _currentPageIsAnAttemptPage() {
        return this._getCurrentPageName().localeCompare('page-mod-quiz-attempt') == 0;
    }

    _currentPageIsSummaryPage() {
        return this._getCurrentPageName().localeCompare('page-mod-quiz-summary') == 0;
    }

    _getNowTimestamp() {
        return Math.floor(Date.now() / 1000);
    }

    _getTimePerPage(result) {

        let timePerPage = 1;
        result.html().find('.que').each((idx, elem) => {
            timePerPage += parseInt(__ENV.MOODLE_QUIZ_MIN_TIME_PER_QUESTION) + Math.random() * parseInt(__ENV.MOODLE_QUIZ_MAX_TIME_PER_QUESTION);
        });

        return timePerPage;

        return parseInt(__ENV.MOODLE_QUIZ_MIN_TIME_PER_PAGE) 
                + Math.ceil(
                    Math.random() * (parseInt(__ENV.MOODLE_QUIZ_MAX_TIME_PER_PAGE) - parseInt(__ENV.MOODLE_QUIZ_MIN_TIME_PER_PAGE))
                );
    }

    _getCurrentPageName() {
        return this.currentPage.find('body').attr('id');
    }

    _getContextId() {
        let bodyClasses = this.currentPage.find('body').attr('class');
        let groups = /context-([0-9]+)/.exec(bodyClasses);

        return groups[1];
    }

    setNextPageButtonCb(callback) {
        this.nextPageButtonCb = callback;
        return this;
    }

    setLoadPageCb(callback) {
        this.loadPageCb = callback;
        return this;
    }
}

export { Quiz };
