/**
 * This class generates answers for quizzes.
 */
class QuizResponder {

    constructor(pageResponse) {
        this.pageResponse = pageResponse;
        this.pageHtml = pageResponse.html();
    }

    getResponses() {

        let fields = {};

        // multianswer.
        this.pageHtml.find('.que.multianswer .answer input[type=text]').each((idx, elem) => {
            fields[elem.getAttribute('name')] = 'guayask';
        });
        this.pageHtml.find('.que.multianswer .formulation .subquestion select').each((idx, elem) => {
            fields[elem.getAttribute('name')] = 1;
        });
        this.pageHtml.find('.que.multianswer .formulation .subquestion input[type=text]').each((idx, elem) => {
            fields[elem.getAttribute('name')] = "Nací un 8 de Enero de 1980";
        });

        // calculated.
        this.pageHtml.find('.que.calculated .formulation .answer input').each((idx, elem) => {
            fields[elem.getAttribute('name')] = 128;
        });

        // calculatedmulti.
        const firstRadio = this.pageHtml.find('.que.calculatedmulti .formulation .answer:has(input[type=radio]) input[type=radio]').get(0);
        if (typeof firstRadio != 'undefined') {
            fields[firstRadio.getAttribute('name')] = firstRadio.getAttribute('value');
        }

        // dd.
        this.pageHtml.find('.que.ddimageortext .ddarea input[type=hidden].placeinput').each((idx, elem) => {
            let dragClass = elem.getAttribute('class');
            let place = dragClass.match(/ place([0-9]+) /)[1].trim();
            fields[elem.getAttribute('name')] = place;
        });
        this.pageHtml.find('.que.ddwtos .formulation input[type=hidden].placeinput').each((idx, elem) => {
            let dragClass = elem.getAttribute('class');
            let place = dragClass.match(/ place([0-9]+) /)[1].trim();
            fields[elem.getAttribute('name')] = place;
        });

        // truefalse
        this.pageHtml.find('.que.truefalse .answer input[type=radio]').each((idx, elem) => {
            fields[elem.getAttribute('name')] = 1;
        });

        // match.
        this.pageHtml.find('.que.match .answer select').each((idx, elem) => {
            fields[elem.getAttribute('name')] = 1;
        });

        // multichoice.
        this.pageHtml.find('.que.multichoice .answer input[type=radio]').each((idx, elem) => {
            fields[elem.getAttribute('name')] = 1;
        });

        // multichoice.
        this.pageHtml.find('.que.multichoice input[type=checkbox]').each((idx, elem) => {
            fields[elem.getAttribute('name')] = 1;
        });

        // numerical.
        this.pageHtml.find('.que.numerical input[type=text]').each((idx, elem) => {
            fields[elem.getAttribute('name')] = "lkjasdklj l akdsj lkajer oiuer";
        });

        // gapselect.
        this.pageHtml.find('.que.gapselect .control select').each((idx, elem) => {
            fields[elem.getAttribute('name')] = "1";
        });

        // essay.
        this.pageHtml.find('.que.essay .answer textarea').each((idx, elem) => {
            fields[elem.getAttribute('name')] = 
             "Sed in id imperdiet. Turpis magna vivamus neque malesuada ornare, vulputate himenaeos tristique tellus nostra? Litora integer scelerisque tempus habitant sem lacinia, feugiat vel rhoncus metus. Dis mattis nisl faucibus vel erat non. Sem vitae suspendisse elementum class. Fusce porta facilisis sit dis et cubilia praesent. Mi nullam mus ac sit. Cubilia venenatis tristique eros blandit cum sagittis et pulvinar felis nullam arcu. Sollicitudin suspendisse fermentum pharetra mattis sociis curae; a non aliquam dictum. Sagittis risus mus platea habitasse taciti torquent."
             + "Odio nascetur duis rutrum elit dapibus euismod cum magnis natoque porttitor vulputate. Mattis suspendisse, et maecenas blandit. Sociis litora consequat himenaeos. Sit risus eleifend dapibus mollis eget. Luctus quis lobortis lectus accumsan cras parturient habitant mollis odio? Vivamus curae; turpis posuere feugiat velit dignissim duis mi. Nisi ornare senectus sed. Pharetra posuere dapibus odio habitant venenatis per. Taciti class sollicitudin condimentum lacus dis sagittis sit varius? Eget nullam posuere."
            + "Tristique ullamcorper montes non habitant! A metus accumsan mus mauris lobortis. A tempus justo diam odio dis curabitur laoreet vivamus montes tincidunt praesent netus. Pretium curabitur, nam massa senectus consequat dignissim ridiculus ut in suspendisse eros nostra. Risus ornare vestibulum maecenas pulvinar donec dictumst. Dolor mauris, interdum nisi? Primis id placerat cursus mauris maecenas nam sociis eleifend eleifend. Duis sociis nisl sollicitudin augue sagittis laoreet felis dictum congue egestas hac. Vestibulum rutrum condimentum ipsum. "
            + "Donec vulputate morbi ante consequat ridiculus urna nam eros. Tortor leo feugiat sociosqu aliquet sollicitudin nostra nisl imperdiet. Quam sociosqu sagittis tristique! Sit erat, sem convallis risus aliquam congue non. Interdum leo ridiculus imperdiet amet justo eros. Class proin, posuere venenatis. Orci tempor ultrices leo vehicula sagittis massa. Diam magna class mi rutrum nostra convallis varius velit eget! Est senectus proin feugiat facilisis malesuada."
            + "Tristique ullamcorper montes non habitant! A metus accumsan mus mauris lobortis. A tempus justo diam odio dis curabitur laoreet vivamus montes tincidunt praesent netus. Pretium curabitur, nam massa senectus consequat dignissim ridiculus ut in suspendisse eros nostra. Risus ornare vestibulum maecenas pulvinar donec dictumst. Dolor mauris, interdum nisi? Primis id placerat cursus mauris maecenas nam sociis eleifend eleifend. Duis sociis nisl sollicitudin augue sagittis laoreet felis dictum congue egestas hac. Vestibulum rutrum condimentum ipsum. "
            + "Donec vulputate morbi ante consequat ridiculus urna nam eros. Tortor leo feugiat sociosqu aliquet sollicitudin nostra nisl imperdiet. Quam sociosqu sagittis tristique! Sit erat, sem convallis risus aliquam congue non. Interdum leo ridiculus imperdiet amet justo eros. Class proin, posuere venenatis. Orci tempor ultrices leo vehicula sagittis massa. Diam magna class mi rutrum nostra convallis varius velit eget! Est senectus proin feugiat facilisis malesuada."
            + "Tristique ullamcorper montes non habitant! A metus accumsan mus mauris lobortis. A tempus justo diam odio dis curabitur laoreet vivamus montes tincidunt praesent netus. Pretium curabitur, nam massa senectus consequat dignissim ridiculus ut in suspendisse eros nostra. Risus ornare vestibulum maecenas pulvinar donec dictumst. Dolor mauris, interdum nisi? Primis id placerat cursus mauris maecenas nam sociis eleifend eleifend. Duis sociis nisl sollicitudin augue sagittis laoreet felis dictum congue egestas hac. Vestibulum rutrum condimentum ipsum. "
            + "Donec vulputate morbi ante consequat ridiculus urna nam eros. Tortor leo feugiat sociosqu aliquet sollicitudin nostra nisl imperdiet. Quam sociosqu sagittis tristique! Sit erat, sem convallis risus aliquam congue non. Interdum leo ridiculus imperdiet amet justo eros. Class proin, posuere venenatis. Orci tempor ultrices leo vehicula sagittis massa. Diam magna class mi rutrum nostra convallis varius velit eget! Est senectus proin feugiat facilisis malesuada."
            + "Tristique ullamcorper montes non habitant! A metus accumsan mus mauris lobortis. A tempus justo diam odio dis curabitur laoreet vivamus montes tincidunt praesent netus. Pretium curabitur, nam massa senectus consequat dignissim ridiculus ut in suspendisse eros nostra. Risus ornare vestibulum maecenas pulvinar donec dictumst. Dolor mauris, interdum nisi? Primis id placerat cursus mauris maecenas nam sociis eleifend eleifend. Duis sociis nisl sollicitudin augue sagittis laoreet felis dictum congue egestas hac. Vestibulum rutrum condimentum ipsum. "
            + "Donec vulputate morbi ante consequat ridiculus urna nam eros. Tortor leo feugiat sociosqu aliquet sollicitudin nostra nisl imperdiet. Quam sociosqu sagittis tristique! Sit erat, sem convallis risus aliquam congue non. Interdum leo ridiculus imperdiet amet justo eros. Class proin, posuere venenatis. Orci tempor ultrices leo vehicula sagittis massa. Diam magna class mi rutrum nostra convallis varius velit eget! Est senectus proin feugiat facilisis malesuada."
            + "Tristique ullamcorper montes non habitant! A metus accumsan mus mauris lobortis. A tempus justo diam odio dis curabitur laoreet vivamus montes tincidunt praesent netus. Pretium curabitur, nam massa senectus consequat dignissim ridiculus ut in suspendisse eros nostra. Risus ornare vestibulum maecenas pulvinar donec dictumst. Dolor mauris, interdum nisi? Primis id placerat cursus mauris maecenas nam sociis eleifend eleifend. Duis sociis nisl sollicitudin augue sagittis laoreet felis dictum congue egestas hac. Vestibulum rutrum condimentum ipsum. "
            + "Donec vulputate morbi ante consequat ridiculus urna nam eros. Tortor leo feugiat sociosqu aliquet sollicitudin nostra nisl imperdiet. Quam sociosqu sagittis tristique! Sit erat, sem convallis risus aliquam congue non. Interdum leo ridiculus imperdiet amet justo eros. Class proin, posuere venenatis. Orci tempor ultrices leo vehicula sagittis massa. Diam magna class mi rutrum nostra convallis varius velit eget! Est senectus proin feugiat facilisis malesuada."
            + "Tristique ullamcorper montes non habitant! A metus accumsan mus mauris lobortis. A tempus justo diam odio dis curabitur laoreet vivamus montes tincidunt praesent netus. Pretium curabitur, nam massa senectus consequat dignissim ridiculus ut in suspendisse eros nostra. Risus ornare vestibulum maecenas pulvinar donec dictumst. Dolor mauris, interdum nisi? Primis id placerat cursus mauris maecenas nam sociis eleifend eleifend. Duis sociis nisl sollicitudin augue sagittis laoreet felis dictum congue egestas hac. Vestibulum rutrum condimentum ipsum. "
            + "Donec vulputate morbi ante consequat ridiculus urna nam eros. Tortor leo feugiat sociosqu aliquet sollicitudin nostra nisl imperdiet. Quam sociosqu sagittis tristique! Sit erat, sem convallis risus aliquam congue non. Interdum leo ridiculus imperdiet amet justo eros. Class proin, posuere venenatis. Orci tempor ultrices leo vehicula sagittis massa. Diam magna class mi rutrum nostra convallis varius velit eget! Est senectus proin feugiat facilisis malesuada."
            + "Tristique ullamcorper montes non habitant! A metus accumsan mus mauris lobortis. A tempus justo diam odio dis curabitur laoreet vivamus montes tincidunt praesent netus. Pretium curabitur, nam massa senectus consequat dignissim ridiculus ut in suspendisse eros nostra. Risus ornare vestibulum maecenas pulvinar donec dictumst. Dolor mauris, interdum nisi? Primis id placerat cursus mauris maecenas nam sociis eleifend eleifend. Duis sociis nisl sollicitudin augue sagittis laoreet felis dictum congue egestas hac. Vestibulum rutrum condimentum ipsum. "
            + "Donec vulputate morbi ante consequat ridiculus urna nam eros. Tortor leo feugiat sociosqu aliquet sollicitudin nostra nisl imperdiet. Quam sociosqu sagittis tristique! Sit erat, sem convallis risus aliquam congue non. Interdum leo ridiculus imperdiet amet justo eros. Class proin, posuere venenatis. Orci tempor ultrices leo vehicula sagittis massa. Diam magna class mi rutrum nostra convallis varius velit eget! Est senectus proin feugiat facilisis malesuada."
            + "Tristique ullamcorper montes non habitant! A metus accumsan mus mauris lobortis. A tempus justo diam odio dis curabitur laoreet vivamus montes tincidunt praesent netus. Pretium curabitur, nam massa senectus consequat dignissim ridiculus ut in suspendisse eros nostra. Risus ornare vestibulum maecenas pulvinar donec dictumst. Dolor mauris, interdum nisi? Primis id placerat cursus mauris maecenas nam sociis eleifend eleifend. Duis sociis nisl sollicitudin augue sagittis laoreet felis dictum congue egestas hac. Vestibulum rutrum condimentum ipsum. "
            + "Donec vulputate morbi ante consequat ridiculus urna nam eros. Tortor leo feugiat sociosqu aliquet sollicitudin nostra nisl imperdiet. Quam sociosqu sagittis tristique! Sit erat, sem convallis risus aliquam congue non. Interdum leo ridiculus imperdiet amet justo eros. Class proin, posuere venenatis. Orci tempor ultrices leo vehicula sagittis massa. Diam magna class mi rutrum nostra convallis varius velit eget! Est senectus proin feugiat facilisis malesuada."
            + "Tristique ullamcorper montes non habitant! A metus accumsan mus mauris lobortis. A tempus justo diam odio dis curabitur laoreet vivamus montes tincidunt praesent netus. Pretium curabitur, nam massa senectus consequat dignissim ridiculus ut in suspendisse eros nostra. Risus ornare vestibulum maecenas pulvinar donec dictumst. Dolor mauris, interdum nisi? Primis id placerat cursus mauris maecenas nam sociis eleifend eleifend. Duis sociis nisl sollicitudin augue sagittis laoreet felis dictum congue egestas hac. Vestibulum rutrum condimentum ipsum. "
            + "Donec vulputate morbi ante consequat ridiculus urna nam eros. Tortor leo feugiat sociosqu aliquet sollicitudin nostra nisl imperdiet. Quam sociosqu sagittis tristique! Sit erat, sem convallis risus aliquam congue non. Interdum leo ridiculus imperdiet amet justo eros. Class proin, posuere venenatis. Orci tempor ultrices leo vehicula sagittis massa. Diam magna class mi rutrum nostra convallis varius velit eget! Est senectus proin feugiat facilisis malesuada."
            + "Tristique ullamcorper montes non habitant! A metus accumsan mus mauris lobortis. A tempus justo diam odio dis curabitur laoreet vivamus montes tincidunt praesent netus. Pretium curabitur, nam massa senectus consequat dignissim ridiculus ut in suspendisse eros nostra. Risus ornare vestibulum maecenas pulvinar donec dictumst. Dolor mauris, interdum nisi? Primis id placerat cursus mauris maecenas nam sociis eleifend eleifend. Duis sociis nisl sollicitudin augue sagittis laoreet felis dictum congue egestas hac. Vestibulum rutrum condimentum ipsum. "
            + "Donec vulputate morbi ante consequat ridiculus urna nam eros. Tortor leo feugiat sociosqu aliquet sollicitudin nostra nisl imperdiet. Quam sociosqu sagittis tristique! Sit erat, sem convallis risus aliquam congue non. Interdum leo ridiculus imperdiet amet justo eros. Class proin, posuere venenatis. Orci tempor ultrices leo vehicula sagittis massa. Diam magna class mi rutrum nostra convallis varius velit eget! Est senectus proin feugiat facilisis malesuada."
            + "Tristique ullamcorper montes non habitant! A metus accumsan mus mauris lobortis. A tempus justo diam odio dis curabitur laoreet vivamus montes tincidunt praesent netus. Pretium curabitur, nam massa senectus consequat dignissim ridiculus ut in suspendisse eros nostra. Risus ornare vestibulum maecenas pulvinar donec dictumst. Dolor mauris, interdum nisi? Primis id placerat cursus mauris maecenas nam sociis eleifend eleifend. Duis sociis nisl sollicitudin augue sagittis laoreet felis dictum congue egestas hac. Vestibulum rutrum condimentum ipsum. "
            + "Donec vulputate morbi ante consequat ridiculus urna nam eros. Tortor leo feugiat sociosqu aliquet sollicitudin nostra nisl imperdiet. Quam sociosqu sagittis tristique! Sit erat, sem convallis risus aliquam congue non. Interdum leo ridiculus imperdiet amet justo eros. Class proin, posuere venenatis. Orci tempor ultrices leo vehicula sagittis massa. Diam magna class mi rutrum nostra convallis varius velit eget! Est senectus proin feugiat facilisis malesuada."
            + "Tristique ullamcorper montes non habitant! A metus accumsan mus mauris lobortis. A tempus justo diam odio dis curabitur laoreet vivamus montes tincidunt praesent netus. Pretium curabitur, nam massa senectus consequat dignissim ridiculus ut in suspendisse eros nostra. Risus ornare vestibulum maecenas pulvinar donec dictumst. Dolor mauris, interdum nisi? Primis id placerat cursus mauris maecenas nam sociis eleifend eleifend. Duis sociis nisl sollicitudin augue sagittis laoreet felis dictum congue egestas hac. Vestibulum rutrum condimentum ipsum. "
            + "Donec vulputate morbi ante consequat ridiculus urna nam eros. Tortor leo feugiat sociosqu aliquet sollicitudin nostra nisl imperdiet. Quam sociosqu sagittis tristique! Sit erat, sem convallis risus aliquam congue non. Interdum leo ridiculus imperdiet amet justo eros. Class proin, posuere venenatis. Orci tempor ultrices leo vehicula sagittis massa. Diam magna class mi rutrum nostra convallis varius velit eget! Est senectus proin feugiat facilisis malesuada."
            + "Tristique ullamcorper montes non habitant! A metus accumsan mus mauris lobortis. A tempus justo diam odio dis curabitur laoreet vivamus montes tincidunt praesent netus. Pretium curabitur, nam massa senectus consequat dignissim ridiculus ut in suspendisse eros nostra. Risus ornare vestibulum maecenas pulvinar donec dictumst. Dolor mauris, interdum nisi? Primis id placerat cursus mauris maecenas nam sociis eleifend eleifend. Duis sociis nisl sollicitudin augue sagittis laoreet felis dictum congue egestas hac. Vestibulum rutrum condimentum ipsum. "
            + "Donec vulputate morbi ante consequat ridiculus urna nam eros. Tortor leo feugiat sociosqu aliquet sollicitudin nostra nisl imperdiet. Quam sociosqu sagittis tristique! Sit erat, sem convallis risus aliquam congue non. Interdum leo ridiculus imperdiet amet justo eros. Class proin, posuere venenatis. Orci tempor ultrices leo vehicula sagittis massa. Diam magna class mi rutrum nostra convallis varius velit eget! Est senectus proin feugiat facilisis malesuada.";
        });

        return fields;
    }

}

export { QuizResponder };
