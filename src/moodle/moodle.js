/**
 * Este módulo se encarga de gestionar la autenticación de los usuarios. Para los usuarios de test, utilizaremos 
 * autenticación interna de Moodle. No pasaremos por el SSO.
 */

import http, { OCSP_STATUS_SERVER_FAILED, file } from 'k6/http';
import { check, sleep, fail } from 'k6';
import { parseHTML } from 'k6/html';
import { Quiz } from './quiz.js';
import { Assign } from './assign.js';
import { Utils } from '../utils/utils.js';
import { Forum } from './forum.js';

class moodle {

    constructor(baseurl) {
        this.baseurl = baseurl;
        this.sesskey = null;

        this.username = null;
        this.lastResult = null;

        this.lastCourseId = null;
        this.inprogresscourses = [];
    }

    getBaseUrl() {
        return this.baseurl;
    }

    visitCourse(courseid) {
        this.lastResult = http.get(this.baseurl + '/course/view.php?id=' + courseid);
        check(this.lastResult, {
            '200 status': (r) => r.status == 200,
            'I\'m in course page': (r) => {
                let bodyClass = r.html().find('body').attr('class');
                return bodyClass.match(new RegExp(' course-' + courseid + ' '));
            }
        });

        http.batch([
            this.getFetchNotificationsRequest(this._getContextId()),
        ]);
    }

    returnToCourse() {
        let courseId = this.lastResult.html().find('header nav .breadcrumb-item a[href*=course]').attr('href');
        courseId = courseId.match(/course\/view.php\?id=([0-9]+)/)[1];
        this.visitCourse(courseId);
    }

    getQuizInstance(cmid) {
        return new Quiz(this, cmid);
    }

    getAssignInstance(cmid) {
        return new Assign(this, cmid);
    }

    getFetchNotificationsRequest() {
        return [
            'POST',
            this.baseurl + "/lib/ajax/service.php?sesskey=" + this.sesskey + "&info=core_fetch_notifications",
            "[{\"index\":0,\"methodname\":\"core_fetch_notifications\",\"args\":{\"contextid\": " + this._getContextId() + "}}]",
        ];
    }

    fetchNotifications(contextid) {
        return http.post(this.baseurl + "/lib/ajax/service.php?sesskey=" + this.sesskey + "&info=core_fetch_notifications",
            "[{\"index\":0,\"methodname\":\"core_fetch_notifications\",\"args\":{\"contextid\": " + contextid + "}}]"
        );
    }

    fetchInProgressCourses() {
        let response = http.post(this.baseurl + "/lib/ajax/service.php?sesskey=" + this.sesskey + "&info=core_course_get_enrolled_courses_by_timeline_classification",
            "[{\"index\":0,\"methodname\":\"core_course_get_enrolled_courses_by_timeline_classification\",\"args\":{\"offset\":0,\"limit\":96,\"classification\":\"inprogress\",\"sort\":\"fullname\"}}]",
        );
        this.inprogresscourses = response.json("0.data.courses.#.id");
        return response;
    }

    login(username, password) {

        this.visitMainPage();
        this.lastResult = http.get(this.baseurl + "/login/index.php");
        check(
            this.lastResult, {
            "is 200 status": r => r.status == 200,
        });

        const logintoken = parseHTML(this.lastResult.body).find("#login [name=logintoken]").val();
        const loginForm = {
            username,
            password,
            logintoken
        };

        Utils.sleep().from(1).to(5).seconds();

        this.lastResult = http.post(this.baseurl + "/login/index.php", loginForm);
        check(
            this.lastResult, {
            "Estado 200": r => r.status == 200,
            "I'm on the main page": r => /\/my\//.exec(r.url)
        });

        let aux = /,"sesskey":"([^"]+)"/.exec(this.lastResult.body);
        this.sesskey = aux[1];
        this.username = username;

        return this;
    }

    visitMainPage() {
        // Página principal.
        this.lastResult = http.get(this.baseurl);
        check(
            this.lastResult, {
            "is 200 status": r => r.status == 200
        });
    }

    visitDashBoard() {
        this.lastResult = http.get(this.baseurl + '/my');
        check(
            this.lastResult, {
            'is 200 status': r => r.status == 200,
        });

        http.batch([
            this.getFetchNotificationsRequest()
        ]);
    }

    visitRandomCourse() {
        let courseid = this.inprogresscourses[Math.ceil(Math.random() * (this.inprogresscourses.length - 1))];
        this.visitCourse(courseid);
        return courseid;
    }

    visitRandomActivity() {
        const numActivities = this.lastResult.html().find('.course-content li.activity .activityinstance').size();
        check(numActivities, {
            'Course has activities': (numActivities) => numActivities > 0,
        });
        const randomActivity = Math.floor(numActivities * Math.random());
        const activityUrl = this.lastResult.html().find('.course-content li.activity .activityinstance a').get(randomActivity).getAttribute('href');
        this.lastResult = http.get(activityUrl);

        check(this.lastResult, {
            '200 status': (r) => r.status == 200,
        });

        const bodyClass = this.lastResult.html().find('body').attr('class');
        if (typeof bodyClass != 'undefined' && bodyClass.match(/ path-mod /)) {
            http.batch([
                this.getFetchNotificationsRequest()
            ]);
        }
    }

    getRandomForum() {
        this.lastHtml = this.lastResult.html();

        let forums = this.lastHtml.find('li.activity.forum');
        if (forums.size() == 0) {
            return null;
        }

        let itemChosen = Math.floor(Math.random() * forums.size());
        let cmid = this.lastHtml.find('li.activity.forum').eq(itemChosen).attr('id').match(/module-([0-9]+)/)[1];

        return new Forum(this, cmid);
    }

    getUsername() {
        return this.username;
    }

    downloadRandomFile() {
        const files = this.lastResult.html().find('.activity.resource .activityinstance a .instancename').filter((idx, element) => {
            return element.text().match(/file/);
        });

        if (files.size() == 0) {
            return;
        }

        const randomFileIndex = Math.floor(Math.random() * (files.size() - 1));
        const href = files.eq(randomFileIndex).closest('a').attr('href');
        this.lastResult = http.get(href, {
            responseType: "none"
        });
        check(this.lastResult, {
            '200 code': (r) => r.status == 200,
        });
    }

    logout() {
        this.lastResult = http.get(this.baseurl + '/login/logout.php?sesskey=' + this.sesskey);
    }

    _getContextId() {
        return this.lastResult.html().find('body').attr('class').match(/ context-([0-9]+)/)[1];
    }

    imInCoursePage() {
        return this.lastResult.html().find('body').attr('class').match(/ path-course-view /);
    }
}

export { moodle };
