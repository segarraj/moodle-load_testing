import { check, sleep, fail } from "k6";
import http from 'k6/http';
import { Activity } from "./activity.js";
import { UploadManager } from "./uploadmanager.js";

class Assign extends Activity {

    constructor(moodle, cmid) {
        super();
        this.moodle = moodle;
        this.baseUrl = moodle.getBaseUrl();
        this.cmid = cmid;

        this.lastResult = null;

        this.sesskey = null;
        this.contextid = null;
    }

    viewAssignment() {

        this.lastResult = http.get(this.baseUrl + '/mod/assign/view.php?id=' + this.cmid);
        check(this.lastResult, {
            "200 code": r => this.lastResult.status == 200,
            "Is page-mod-assign-view": r => this.getPageName() == 'page-mod-assign-view',
        });

        return this;
    }

    addSubmission(filename) {
        this._clickAddSubmission()
        this._submitFile(filename);
    }

    _clickAddSubmission() {
        this.lastResult = this.lastResult.submitForm({
            formSelector: '.submissionstatustable .submissionaction form:has(input[name=action][value=editsubmission])'
        });

        check(this.lastResult, {
            "200 status": (r) => r.status == 200,
            "I'm in page-mod-assign-editsubmission": (r) => this.getPageName() == 'page-mod-assign-editsubmission',
        });
    }

    _submitFile(fileToUpload) {
        // Search for the itemid value in code.
        let fileManagerId = this.lastResult.html().find('#id_files_filemanager').attr('value');
        let uploadManager = new UploadManager(this.moodle, fileManagerId);

        let result = uploadManager.uploadFile(fileToUpload, "fichero.data");
        this.lastResult = this.lastResult.submitForm({
            formSelector: '.editsubmissionform form.mform',
            fields: {
                files_filemanager: result.id,
            },
            submitSelector: '[name=submitbutton]'
        });

        check(this.lastResult, {
            "200 code": (r) => r.status == 200,
        });
    }
}

export { Assign };
