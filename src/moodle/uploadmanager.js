import { check, fail, sleep } from "k6";
import http from 'k6/http';


class UploadedFileInfo {

    constructor(id, url, file) {
        this.id = id;
        this.url = url;
        this.file = file;
    }
}

class UploadManager {

    constructor(moodle, fileManagerId   ) {
        this.baseUrl = moodle.getBaseUrl();
        this.moodle = moodle;
        this.fileManagerId = fileManagerId;
    }

    uploadFile(file, fileName) {

        this._listRepositories();

        sleep(Math.ceil(2 * Math.random()));

        let result = http.post(this.baseUrl + '/repository/repository_ajax.php?action=upload', {
            repo_upload_file: http.file(file, fileName),
            itemid: this.fileManagerId,
            repo_id: 4,
            sesskey: this.moodle.sesskey,
        });

        check(result, {
            "200 status": (r) => r.status == 200,
            "Moodle does not return json error": (r) => !r.json("error"), 
        });

        let error = result.json("error");
        if (error) {
            fail(error);
        }

        let responseJson = result.json();
        return new UploadedFileInfo(responseJson.id, responseJson.url, responseJson.file);
    }

    _listRepositories() {
        let result = http.post(this.baseUrl + '/repository/repository_ajax.php?action=list', {
            'sesskey': this.moodle.sesskey,
            'repo_id': 4,
            'itemid': this.fileManagerId,
        });

        check(result, {
            "200 code": (r) => r.status == 200,
        });
    }
}

export { UploadManager, UploadedFileInfo };
